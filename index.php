<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S3: Classes and Objects</title>
</head>
<body>
	<h3>Person</h3>

	<p><?= $person->printName(); ?></p>

	<h3>Developer</h3>

	<p><?= $developer->printName(); ?></p>

	<h3>Engineer</h3>

	<p><?= $engineer->printName(); ?></p>
</body>
</html>
